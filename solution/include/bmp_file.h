#ifndef BMP_FILE_H 
#define BMP_FILE_H
#include "bmp_image.h"
#include "file_origin.h"
#include "file_status.h"
#include <stdlib.h>

struct __attribute__((packed))bmp_header {
    uint16_t bfType; 
    uint32_t bfileSize; 
    uint32_t bfReserved; 
    uint32_t bOffBits; 
    uint32_t biSize; 
    uint32_t biWidth; 
    uint32_t biHeight; 
    uint16_t biPlanes; 
    uint16_t biBitCount; 
    uint32_t biCompression; 
    uint32_t biSizeImage; 
    uint32_t biXPelsPerMeter; 
    uint32_t biYPelsPerMeter; 
    uint32_t biClrUsed; 
    uint32_t biClrImportant; 
};

enum read_status read_from_bmp( FILE* input, struct image* image );

enum write_status write_to_bmp( FILE* output, struct image* image );

#endif //BMP_FILE_H
