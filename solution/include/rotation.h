#include "bmp_file.h"
#include "bmp_image.h"
#include <stdio.h>
#ifndef IMAGE_ROTATION_H
#define IMAGE_ROTATION_H

void rotation (struct image const *origin, struct image *new_image);

#endif //IMAGE_ROTATION_H
