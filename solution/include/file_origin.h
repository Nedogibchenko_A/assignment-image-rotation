#ifndef FILE_ORIGIN_H 
#define FILE_ORIGIN_H
#include <stdio.h>

FILE * open_for_read(const char* file_name);

FILE * open_for_write(const char* file_name);

int close(FILE* file_name);

#endif //FILE_ORIGIN_H
