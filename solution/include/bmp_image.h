#ifndef BMP_IMAGE_H
#define BMP_IMAGE_H
#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image (uint64_t const width, uint64_t const height);

void free_image(struct image* image);

#endif //BMP_IMAGE_H
