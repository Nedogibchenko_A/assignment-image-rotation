#include "../include/bmp_file.h" 
#include "../include/file_origin.h"

FILE* open_for_read(const char* file_name){
    return fopen(file_name, "rb");
}

FILE* open_for_write(const char* file_name){
    return fopen(file_name, "wb");
}
int close(FILE* file_name){
    return fclose(file_name);
}
