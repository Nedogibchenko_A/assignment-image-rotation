#include "../include/rotation.h" 

static size_t turn_against_90(struct image* new_image, size_t i, size_t j){
    size_t result = i*new_image->width+j;
    return result;
}

static struct pixel get_pixel(struct image const* origin, size_t i, size_t j){
    return origin->data[(origin->height-j-1)*origin->width+i];
} 

void rotation (struct image const *origin, struct image *new_image){
    *new_image = create_image(origin->height, origin->width);

    for (size_t i = 0; i < origin->width; i++) {
        for (size_t j = 0; j < origin->height; j++) {
            new_image->data[turn_against_90(new_image, i, j)] = get_pixel(origin, i, j);
        }
    }
}
