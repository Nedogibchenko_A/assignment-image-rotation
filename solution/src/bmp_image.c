#include "../include/bmp_image.h"
#include <stdlib.h>

struct image create_image(uint64_t const width, uint64_t const height){
    return (struct image ){.width = width, .height = height, .data = malloc(sizeof(struct pixel) * width * height)};
}

void free_image(struct image* image){
    if (image){
        free(image->data);
    }
}
