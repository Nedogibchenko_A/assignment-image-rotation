#include "../include/rotation.h" 
#include "../include/file_status.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; 
    (void) argv; 
    if (argc != 3) {
        fprintf(stderr, "Invalid number");
        return 1;
    }

    FILE* file_1 = open_for_read(argv[1]);
    if (!file_1){
        fprintf(stderr, "No file to read");
        return 1;
    }

    struct image image = {0};

    if(read_from_bmp(file_1, &image) != READ_SUCCESSFULLY){
        close(file_1);
        fprintf(stderr, "Problem with reading");
        return 1;
    }

    struct image image_2 = {0};

    rotation(&image, &image_2);

    free_image(&image);

    FILE *file_2 = open_for_write(argv[2]);
    if (!file_2){
        fprintf(stderr, "No file to write");
        return 1;
    }

    if(write_to_bmp(file_2, &image_2) != WRITE_SUCCESSFULLY){
        close(file_2);
        fprintf(stderr, "Problem with writing");
        return 1;
    }

    free_image(&image_2);
}
