#include "../include/bmp_file.h"
#include "../include/bmp_image.h"
#include "../include/file_status.h"
#define BMP_BF_TYPE 0x4D42 
#define BMP_BI_SIZE 40 
#define BMP_BI_BITCOUNT 24 
#define BMP_PLANE 1
#define BMP_ZERO 0

static struct bmp_header create_bmp(const uint64_t width, const uint64_t height) {
    struct bmp_header bmp_header = {
            .bfType = BMP_BF_TYPE, 
            .bfileSize =  sizeof(struct bmp_header) + width * height * sizeof(struct pixel) + height * (width % 4), 
            .bfReserved = BMP_ZERO, 
            .bOffBits = sizeof(struct bmp_header), 
            .biSize = BMP_BI_SIZE, 
            .biWidth = width, 
            .biHeight = height,  
            .biPlanes = BMP_PLANE, 
            .biBitCount = BMP_BI_BITCOUNT, 
            .biCompression = BMP_ZERO, 
            .biSizeImage =  height * width * sizeof(struct pixel) + (width % 4) * height, 
            .biXPelsPerMeter = BMP_ZERO, 
            .biYPelsPerMeter = BMP_ZERO, 
            .biClrUsed = BMP_ZERO, 
            .biClrImportant = BMP_ZERO,  
    };
    return bmp_header;
}

static uint8_t get_padding(uint32_t const width) {
    return width % 4;
}

static size_t image_row_addres(const size_t row, struct image *image){
        size_t x = row * image->width;
        return x; 
    }

static enum read_status check_header(struct bmp_header *const header) {
    if (header->bfType != BMP_BF_TYPE) {
        return READING_ERROR_INVALID_SIGNATURE;
    }
    if (header->biSize != BMP_BI_SIZE) {
        return READING_ERROR_INVALID_HEADER;
    }
    if (header->biBitCount != BMP_BI_BITCOUNT) {
        return READING_ERROR_INVALID_BITS;
    }
    return READ_SUCCESSFULLY;
}

enum read_status read_from_bmp(FILE* input, struct image* image) {

    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, input) == 1) {
        if (check_header(&header) != READ_SUCCESSFULLY) {
            return check_header(&header);
        }
    } else {
        return READING_ERROR;
    }
    
    *image = create_image(header.biWidth, header.biHeight);

    if(!(image->data)){
        return READING_ERROR;
    }

    for (size_t i = 0; i < image->height; i++) {
        if ((fread(&(image->data[image_row_addres(i, image)]), sizeof(struct pixel), image->width, input) != image->width) || (fseek(input, get_padding(image->width), SEEK_CUR) != 0)) {
            return READING_ERROR;
        }
    }
    return READ_SUCCESSFULLY;
}

enum write_status write_to_bmp(FILE* output, struct image* image) {
    const struct bmp_header bmp_header_former = create_bmp(image->width, image->height);
    if (fwrite(&bmp_header_former, sizeof(struct bmp_header), 1, output) == 1) {
        const void* plug = 0;
        for (size_t i = 0; i < image->height; i++) {
            if ((fwrite(&(image->data[image_row_addres(i, image)]), sizeof(struct pixel), image->width, output) != image->width) || (fwrite(&plug, 1, get_padding(image->width), output) != get_padding(image->width))){
                return WRITING_ERROR;
            }
        }
        return WRITE_SUCCESSFULLY;
    }
    return WRITING_ERROR;
}
